DROP TABLE IF EXISTS tweets;
 
CREATE TABLE Tweets(
  id INT AUTO_INCREMENT  PRIMARY KEY,
  usuario VARCHAR(250) NOT NULL,
  texto VARCHAR(250) NOT NULL,
  localizacion VARCHAR(250) DEFAULT NULL,
  validacion BOOLEAN NULL
);
 
INSERT INTO Tweets (id,usuario, texto, localizacion,validacion) VALUES
  (1,'Pipe', 'In the landa, of the...1', 'SPAIN',true),
  (2,'Manu', 'In the lando, of the...2', 'SPAIN',false),
  (3,'Pepe', 'In the landi, of the...3', 'SPAIN',false),
  (4,'Alberto', 'In the lande, of the...4', 'SPAIN',false),
  (5,'Bea', 'In the landu, of the...5', 'SPAIN',false);

  