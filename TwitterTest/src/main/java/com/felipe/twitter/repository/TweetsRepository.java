package com.felipe.twitter.repository;


import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.felipe.twitter.model.Tweets;

public interface TweetsRepository extends CrudRepository<Tweets, Long>{
	@Query("select u from Tweets u where u.texto LIKE CONCAT('%',:texto,'%')")
	public List<Tweets> findUserByTextoLike(@Param("texto") String texto);
	
	
}
