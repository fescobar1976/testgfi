package com.felipe.twitter.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.felipe.twitter.controller.TweetsController;
import com.felipe.twitter.service.TweetsService;
import com.felipe.twitter.source.TwitterReader;

//import com.felipe.twitter.controller.TweetsController;

@SpringBootApplication(scanBasePackages={"com.felipe.*"})
@EnableAutoConfiguration
@EntityScan(basePackages = {"com.felipe.twitter.*"})
@ComponentScan(basePackageClasses = {TweetsController.class,TweetsService.class,TwitterReader.class})
@EnableJpaRepositories(basePackages = "com.felipe.twitter.repository")
public class TwitterApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext appContext =SpringApplication.run(TwitterApplication.class, args);
		TwitterReader reader=appContext.getBean(TwitterReader.class);
		reader.init();
	}

}